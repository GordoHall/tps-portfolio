package com.citi.training.tps.dao;

import com.citi.training.tps.model.Trade;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface TradingMongoDB extends MongoRepository<Trade, String> {


    
}
