package com.citi.training.tps.service;

import java.util.List;
import java.util.Optional;

import com.citi.training.tps.dao.TradingMongoDB;
import com.citi.training.tps.model.Trade;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author Ethan, Sam, Gordon
 */
@Component
public class PortfolioService {

    @Autowired
    private TradingMongoDB tradingMongoDB;


    public List<Trade> findAll(){
        return tradingMongoDB.findAll();
    }
    public Trade create(Trade trade){
        return tradingMongoDB.save(trade);
    }

    public Trade update(String id, Trade trade){
        trade.setId(id);
        return tradingMongoDB.save(trade);
    }
    public Optional<Trade> get(String id) {
        return tradingMongoDB.findById(id);
    }

    public void delete(Trade trade){
         tradingMongoDB.delete(trade);
    }
}
