package com.citi.training.tps.rest;

import java.util.List;
import java.util.Optional;

import com.citi.training.tps.model.Trade;
import com.citi.training.tps.service.PortfolioService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("v1/portfolio")
public class PortfolioController {

    private static final Logger LOG = LoggerFactory.getLogger(PortfolioController.class);

    @Autowired
    PortfolioService portfolioService;

    @RequestMapping(method=RequestMethod.GET)
    public List<Trade> getAll() {
       return portfolioService.findAll();
    }

    @RequestMapping(path="/{id}",method=RequestMethod.GET)
    public ResponseEntity getOne(@PathVariable String id){
        Optional<Trade> trade = portfolioService.get(id);
        return new ResponseEntity<Trade>(trade.get(),HttpStatus.OK);
    }

    @RequestMapping(path="/{id}",method=RequestMethod.PUT)
    public ResponseEntity putOne(@PathVariable String id, @RequestBody Trade trade){
        portfolioService.update(id, trade);
        return new ResponseEntity<Trade>(trade,HttpStatus.OK);
    }
    @RequestMapping(path="/{id}",method=RequestMethod.POST)
    public ResponseEntity postOne(@PathVariable String id, @RequestBody Trade trade){
        portfolioService.update(id, trade);
        return new ResponseEntity<Trade>(trade,HttpStatus.OK);
    }
    @RequestMapping(path="/{id}",method=RequestMethod.DELETE)
    public ResponseEntity deleteOne(@PathVariable String id){
        Optional<Trade> trade= portfolioService.get(id);
        portfolioService.delete(trade.get());
        return new ResponseEntity<Trade>(HttpStatus.valueOf(204));
    }



}
