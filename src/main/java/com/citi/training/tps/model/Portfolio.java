package com.citi.training.tps.model;

import java.util.ArrayList;
import java.util.Map;

public class Portfolio {

    private String id;
    private ArrayList<Trade> history = new ArrayList<Trade>();
    private ArrayList<Map> stockOwned = new ArrayList<>();
    private float credit;


    public Portfolio() {
	}

    public Portfolio(String id, float credit) {
    this.id = id;
    this.credit = credit;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public float getCredit() {
        return credit;
    }

    public void setCredit(float credit) {
        this.credit = credit;
    }



}
