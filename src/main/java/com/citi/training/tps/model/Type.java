package com.citi.training.tps.model;

public enum Type {
    BUY, SELL;
}
