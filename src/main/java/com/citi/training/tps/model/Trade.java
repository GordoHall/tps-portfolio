package com.citi.training.tps.model;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class Trade {

	private static final Logger LOG = LoggerFactory.getLogger(Trade.class);

    private String id;
    private Date dateCreated;
    private String stockTicker;
    private int stockQuantity;
    private double reqPrice;
    private Type type;
    private State state;

    public Trade() {
	}

    public Trade(String id, Date dateCreated, String stockTicker, int stockQuantity, double reqPrice, Type type,
    State state) {
    this.id = id;
    this.dateCreated = dateCreated;
    this.setStockTicker(stockTicker);
    this.stockQuantity = stockQuantity;
    this.reqPrice = reqPrice;
    this.type = type;
    this.setState(state);
}


    public String getId(){
        return id;
    }

    public void setId(String id){
        this.id = id;
    }

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}
	public String getStockTicker() {
		return stockTicker;
	}
	public void setStockTicker(String stockTicker) {
		this.stockTicker.equalsIgnoreCase(stockTicker)
	}
	public int getStockQuantity() {
		return stockQuantity;
	}
	public void setStockQuantity(int stockQuantity) {
		this.stockQuantity = stockQuantity;
	}
	public double getReqPrice() {
		return reqPrice;
	}
	public void setReqPrice(double reqPrice) {
		this.reqPrice = reqPrice;
	}

	public Type getTradeType() {
		return type;
	}
	public void setTradeType(Type tradeType) {
		this.type = tradeType;
	}
	public State getState() {
		return state;
	}
	public void setState(State state) {
		if(stockTicker.equalsIgnoreCase(null)){
			this.state = State.REJECTED;
		} else {
			this.state = state;
		}
		
	}

	@Override
	public String toString() {
		return "Trade [dateCreated=" + dateCreated + ", id=" + id + ", reqPrice=" + reqPrice + ", state=" + state
				+ ", stockQuantity=" + stockQuantity + ", stockTicker=" + stockTicker + ", tradeType=" + type
				+ "]";
	}

    
    
}
